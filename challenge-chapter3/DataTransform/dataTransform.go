package main

import (
	"database/sql"
	"encoding/csv"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	_ "github.com/lib/pq"
	"gopkg.in/yaml.v3"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

type Mentee struct {
	gorm.Model
	Id                int
	Full_name         string
	Birthday          time.Time
	Academic_level    int
	Work_level        float64
	Gender            bool
	Introduce_text    string
	GOLANG_start_date time.Time
}

type YamlConfig struct {
	Development struct {
		Dialect  string `yaml:"dialect"`
		Database string `yaml:"database"`
		Pool     int64  `yaml:"pool"`
		Username string `yaml:"username"`
		Password string `yaml:"password"`
		Host     string `yaml:"host"`
		Port     int64  `yaml:"port"`
	}
}

func readConf(filename string) (*YamlConfig, error) {
	buf, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	var yamlConfig YamlConfig
	err = yaml.Unmarshal(buf, &yamlConfig)
	if err != nil {
		return nil, fmt.Errorf("in file %q: %v", filename, err)
	}

	return &yamlConfig, nil
}

func main() {

	config, err := readConf("postgres.database.yaml")
	if err != nil {
		log.Fatal(err)
	}

	rawData, err := readCsvFile("list-mentee.csv")

	mentees := getMenteesFromCSV(rawData, err)

	fmt.Println("Length of mentees: ", len(mentees))

	// PostgreSQL instane details
	host := config.Development.Host
	port := config.Development.Port
	user := config.Development.Username
	password := config.Development.Password
	dbname := config.Development.Database

	// db connection statement
	dsn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)

	isConnect := connectToDatabase(dsn)

	if isConnect {
		isAddData := addDataToPostgreSQL(dsn, mentees)

		if isAddData {
			fmt.Println("Add data to database successfully")
		} else {
			fmt.Println("Failed to add data to database")
		}
	} else {
		os.Exit(1)
	}
}

func readCsvFile(filepath string) ([][]string, error) {
	csvFile, errOpen := os.Open(filepath)

	if errOpen != nil {
		fmt.Println("Can not open or find file: ", filepath)

		emptyArr := make([][]string, 0)

		return emptyArr, errOpen
	} else {

		defer csvFile.Close()

		csvLines, err := csv.NewReader(csvFile).ReadAll()

		return csvLines, err
	}

}

func getGender(genderStr string) string {
	genderUpperStr := strings.ToUpper(genderStr)

	switch genderUpperStr {
	case "NAM":
	case "1":
	case "TRUE":
		return "TRUE"
	case "FALSE":
	case "0":
	case "NU":
	case "NỮ":
		return "FALSE"
	default:
		fmt.Println("Wrong format of gender")
	}

	return "Wrong format of gender"
}

func getGOLANGStartDate(startDateStr string) time.Time {
	date, err := time.Parse("2006-01-02", startDateStr)

	if err != nil {
		date1, err1 := time.Parse("20060102", startDateStr)

		if err1 != nil {
			date2, err2 := time.Parse("02/01/2006", startDateStr)

			if err2 != nil {
				fmt.Println("Wrong format date")

				return time.Now()
			} else {
				return date2
			}

		} else {
			return date1
		}

	} else {
		return date
	}

	return time.Now()
}

func getMenteesFromCSV(data [][]string, err error) []Mentee {
	menteeData := make([]Mentee, 0)

	if err != nil {
		fmt.Println("File not found")
		fmt.Println(err)
		os.Exit(1)
	}

	for i := 1; i < len(data); i++ {
		id, _ := strconv.Atoi(data[i][0])
		birthday, _ := time.Parse("2006-01-02", data[i][2])
		academicLevel, _ := strconv.Atoi(data[i][3])
		workLevel, _ := strconv.ParseFloat(data[i][4], 64)
		// parse gender
		gender, _ := strconv.ParseBool(getGender(data[i][5]))
		// parse golang start date
		golangStartDate := getGOLANGStartDate(data[i][7])

		mentee := Mentee{
			Id:                id,
			Full_name:         data[i][1],
			Birthday:          birthday,
			Academic_level:    academicLevel,
			Work_level:        workLevel,
			Gender:            gender,
			Introduce_text:    data[i][6],
			GOLANG_start_date: golangStartDate,
		}
		menteeData = append(menteeData, mentee)
	}

	return menteeData
}

func connectToDatabase(dsn string) bool {

	db, err := sql.Open("postgres", dsn)

	// connect fail
	if err != nil {
		panic("Failed to open database")
		fmt.Println(err)

		return false
	} else {
		fmt.Println("Successfully connected to database")
		fmt.Println(db.Ping())
		return true
	}
}

func addDataToPostgreSQL(dsn string, data []Mentee) bool {
	// connect to PostgreSQL
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true,
		},
	})

	if err != nil {
		fmt.Println("Failed to connect PostgreSQL")
		panic(err)

		return false
	} else {
		fmt.Println("Successfully connected to PostgreSQL")

		// migrate the schema to the database
		db.AutoMigrate(&Mentee{})

		// create data
		db.Create(data)

		return true
	}
}
