package main

import (
	"database/sql"
	"encoding/csv"
	"fmt"
	"os"
)

func main() {

	// 1- read CSV file & transform to data
	// 2- connect to database & postgres
	// 3- insert & update data to database
	// 4- export data to CSV file
}

func readCsvFile(filepath string) ([][]string, error) {
	csvFile, errOpen := os.Open(filepath)

	if errOpen != nil {
		fmt.Println("Can not open or find file: ", filepath)

		emptyArr := make([][]string, 0)

		return emptyArr, errOpen
	} else {

		defer csvFile.Close()

		csvLines, err := csv.NewReader(csvFile).ReadAll()

		return csvLines, err
	}
}

func connectToDatabase(dsn string) bool {
	db, err := sql.Open("postgres", dsn)

	// connect failed
	if err != nil {
		fmt.Println("Failed to connect database", err)

		return false
	} else {
		fmt.Println("Successfully to connect database")
		fmt.Println(db.Ping())

		return true
	}
}

func connectToPostgres(dsn string) {

}

func exportToCsvFile(filepath string) {

}
