package main

import (
	"database/sql"
	"encoding/csv"
	"fmt"
	"os"
	"strconv"
	"time"

	_ "github.com/lib/pq"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

type Employee struct {
	gorm.Model
	Id             int
	Full_name      string
	Birthday       time.Time
	Academic_level int
	Work_level     float64
	Gender         bool
	Introduce_text string
}

func main() {
	args := os.Args[1:]

	rawData, err := readCsvFile("employeesFile.csv")

	emps := getEmployeesFromCSV(rawData, err)

	fmt.Println("Length of employees: ", len(emps))

	// PostgreSQL instane details
	host := args[0]
	numPort, _ := strconv.Atoi(args[1])
	port := numPort
	user := args[2]
	password := args[3]
	dbname := args[4]

	// db connection statement
	dsn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)

	isConnect := connectToDatabase(dsn)

	if isConnect {
		addDataToPostgreSQL(dsn, emps)
	} else {
		os.Exit(1)
	}
}

func readCsvFile(filepath string) ([][]string, error) {
	csvFile, errOpen := os.Open(filepath)

	if errOpen != nil {
		fmt.Println("Can not open or find file: ", filepath)

		emptyArr := make([][]string, 0)

		return emptyArr, errOpen
	} else {

		defer csvFile.Close()

		csvLines, err := csv.NewReader(csvFile).ReadAll()

		return csvLines, err
	}

}

func getEmployeesFromCSV(data [][]string, err error) []Employee {
	empData := make([]Employee, 0)

	if err != nil {
		fmt.Println("File not found")
		fmt.Println(err)
		os.Exit(1)
	}

	for i := 1; i < len(data); i++ {
		id, _ := strconv.Atoi(data[i][0])
		birthday, _ := time.Parse("2006-01-02", data[i][2])
		academicLevel, _ := strconv.Atoi(data[i][3])
		workLevel, _ := strconv.ParseFloat(data[i][4], 64)
		gender, _ := strconv.ParseBool(data[i][5])

		emp := Employee{
			Id:             id,
			Full_name:      data[i][1],
			Birthday:       birthday,
			Academic_level: academicLevel,
			Work_level:     workLevel,
			Gender:         gender,
			Introduce_text: data[i][6],
		}
		empData = append(empData, emp)
	}

	return empData
}

func connectToDatabase(dsn string) bool {

	db, err := sql.Open("postgres", dsn)

	// connect fail
	if err != nil {
		panic("Failed to open database")
		fmt.Println(err)

		return false
	} else {
		fmt.Println("Successfully connected to database")
		fmt.Println(db.Ping())
		return true
	}
}

func addDataToPostgreSQL(dsn string, data []Employee) bool {
	// connect to PostgreSQL
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true,
		},
	})

	if err != nil {
		fmt.Println("Failed to connect PostgreSQL")
		panic(err)

		return false
	} else {
		fmt.Println("Successfully connected to PostgreSQL")

		// migrate the schema to the database
		db.AutoMigrate(&Employee{})

		// create data
		db.Create(data)

		return true
	}
}
