[TOC]

# RUN Challenge day1 / chapter 1 programs

## Program 1: Find min, max, mean, median

- Open cmd
- Direct challenge-day1 folder
- Direct program1 folder
- Enter cmd "./program1 array-of-number"
- Ex: ./program1 1 2 9 4 8 2

## Program 2: Print text of number

- Open cmd
- Direct challenge-day1 folder
- Direct program2 folder
- Enter cmd "./program2 number"
- Ex: ./program2 123

## Program 3: Print table of random value of type

- Open cmd
- Direct challenge-day1 folder
- Direct program3 folder
- Enter cmd "./program3 col_name1 type_value1 col_name2 type_value2 ..."
- Type value is one of the following: int (Integer), string (String), float (Float), bool (Boolean)
- Ex: ./program3 col1 int col2 string col3 bool col4 float
- Type string will random string with length is 6 characters.
- Type int will random number between 0 to 999

## Program 4: Sum of two string number

- Open cmd
- Direct challenge-day1 folder
- Direct program4 folder
- Enter cmd "./program4 sum string_number1 string_number2"
- Ex: ./program4 sum 123 45

## Program 5: Count days between two dates

- Open cmd
- Direct challenge-day1 folder
- Direct program5 folder
- Enter cmd "./program5 CountDate date1 date2"
- Date value with format yyyy-MM-dd
- Ex: CountDate 2019-12-31 2020-12-31
- If only 1 date, the program will calculate days between date input to current date.

# RUN Challenge chapter 2 programs

## Program 1: ReadCSV_EmployeeV1

- Open cmd
- Direct challenge-chapter2 folder
- Direct program1 folder
- Enter cmd "go run read_csv1.go csvFileName"
- Ex: go run read_csv1.go empData.csv
- It already contains simple csv file in folder for run program which names empData.csv
- If you have other csv files, please direct path in cmd.

## Program Challenge Chapter#2: TOEIC Exam

- Open cmd
- Direct challenge-chapter2 folder
- Enter cnd "go run challengeChapter2.go"
- The program will display information of:
  - Number of parts of exam
  - Number of question each part
  - List of question and answer options

# RUN Challenge chapter 3 programs

## Program 1: ImportCSV2DB Program v0.0.1

- Open cmd
- Direct to challenge-chapter3 folder
- Direct to ImportCSV2DB folder
- Enter cmd "go run importCSV2DB.go host port user password dbname"
- Ex: go run importCSV2DB.go localhost 5432 user1 123456 my_db
- Explain:
  - host: hostname or IP Address of server PostgreSQL
  - port: default port of PostgreSQL
  - user: default user for postgres database
  - password: password used during initial setup
  - dbname: using default database that come with postgres
- The program will connect to your PostgreSQL, then create table and insert data from csv file in folder to database

## Program 2: DataTransform Program v0.0.1

- Open cmd
- Direct to challenge-chapter3 folder
- Direct to DataTransform folder
- Open postgres.database.yaml file & fill your PostgreSQL configuration
- Enter cmd "go run dataTransform.go host port user password dbname"
- Ex: go run importCSV2DB.go
- The program will read yaml file name postgres.database.yaml to get config for database connection
- The program will read file in folder name "list-mentee.csv", then transform to data and connect and insert to database
