package main

import (
	"fmt"
	"os"
	"sort"
	"strconv"
)

func findMinMax(numbers []string) {
	min, _ := strconv.ParseFloat(numbers[0], 64)
	max, _ := strconv.ParseFloat(numbers[0], 64)

	for i := 2; i < len(numbers); i++ {
		n, _ := strconv.ParseFloat(numbers[i], 64)
		if n < min {
			min = n
		}
		if n > max {
			max = n
		}
	}

	fmt.Println("Min:", min)
	fmt.Println("Max:", max)
}

func findMean(numbers []string) {
	var sum float64 = 0
	for _, number := range numbers {
		n, _ := strconv.ParseFloat(number, 64)
		sum += n
	}
	var mean = sum / float64(len(numbers))
	fmt.Printf("Mean: %.2f", mean)
	fmt.Println()
}

func findMedian(numbers []string) {
	parseNumbers := make([]float64, 0)

	for _, number := range numbers {
		n, _ := strconv.ParseFloat(number, 64)
		parseNumbers = append(parseNumbers, n)
	}

	sort.Float64s(parseNumbers)
	mNumber := len(parseNumbers) / 2

	if len(parseNumbers)%2 == 1 {
		fmt.Printf("Median: %.f", parseNumbers[mNumber])
		fmt.Println()
	} else {
		fmt.Printf("Median: %.1f", (parseNumbers[mNumber-1]+parseNumbers[mNumber])/2)
		fmt.Println()
	}
}

func main() {
	numbers := os.Args[1:]

	findMinMax(numbers)
	findMean(numbers)
	findMedian(numbers)
}
