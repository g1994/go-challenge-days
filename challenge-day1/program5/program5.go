package main

import (
	"fmt"
	"os"
	"time"
)

func main() {
	args := os.Args[1:]
	time1 := args[1]
	var time2 string

	// do not have date2
	if len(args) == 2 {
		time2 = time.Now().Format("2006-01-02")
	} else {
		time2 = args[2]
	}

	date1, _ := time.Parse("2006-01-02", time1)
	date2, _ := time.Parse("2006-01-02", time2)

	// cal days between two dates
	days := date2.Sub(date1).Hours() / 24

	fmt.Printf("Days between two date is %.0f days", days)
	fmt.Println()
}
