package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Stack []string

func (s *Stack) isEmpty() bool {
	return len(*s) == 0
}

func (s *Stack) push(str string) {
	*s = append(*s, str)
}

func (s *Stack) pop() (string, bool) {
	if s.isEmpty() {
		return "", false
	}
	index := len(*s) - 1
	ele := (*s)[index]
	*s = (*s)[:index]

	return ele, true
}

func sumTwoChar(s1, s2 byte) byte {
	// sum by ASCII code
	return s1 - 48 + s2 - 48
}

func sum(s1, s2 string) string {
	listCharS1 := strings.Split(s1, "")
	listCharS2 := strings.Split(s2, "")
	var stackS1 Stack
	var stackS2 Stack
	var resultStr string
	var lenChar int
	var res byte
	isSurplus := false

	for _, char := range listCharS1 {
		stackS1.push(char)
	}
	for _, char := range listCharS2 {
		stackS2.push(char)
	}

	if len(stackS1) > len(stackS2) {
		lenChar = len(stackS1)
	} else {
		lenChar = len(stackS2)
	}

	for i := 0; i < lenChar; i++ {
		c1, isExist1 := stackS1.pop()
		c2, isExist2 := stackS2.pop()
		var b1 []byte
		var b2 []byte

		if isExist1 == true {
			b1 = []byte(c1)
		} else {
			// get byte of 0
			b1 = []byte("0")
		}

		if isExist2 == true {
			b2 = []byte(c2)
		} else {
			// get byte of 0
			b2 = []byte("0")
		}

		res = sumTwoChar(b1[0], b2[0])

		// if have surplus from last calculate
		if isSurplus == true {
			resByte := []byte(strconv.Itoa(int(res)))
			res = sumTwoChar(resByte[0], '1')
		}

		if res > 9 {
			isSurplus = true
		} else {
			isSurplus = false
		}

		resultStr = strconv.Itoa(int(res%10)) + resultStr
	}

	if isSurplus {
		resultStr = "1" + resultStr
	}

	return resultStr
}

func main() {
	args := os.Args[1:]

	fmt.Println(sum(args[1], args[2]))
}
