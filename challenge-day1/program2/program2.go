package main

import (
	"fmt"
	"os"
	"strconv"
)

func splitNumber(num int) []int {
	numDigits := make([]int, 0)
	var tmpNum = num

	for tmpNum > 0 {
		mod := tmpNum % 10
		numDigits = append(numDigits, mod)
		tmpNum = tmpNum / 10
	}

	return numDigits
}

func displayNumberText(numDigits []int) string {
	result := ""
	numberMap := map[int]string{
		1: "một",
		2: "hai",
		3: "ba",
		4: "bốn",
		5: "năm",
		6: "sáu",
		7: "bảy",
		8: "tám",
		9: "chín",
	}

	if len(numDigits) == 0 {
		return "không"
	}

	for i := len(numDigits) - 1; i >= 0; i-- {
		charDigit := numDigits[i]

		switch i {
		// hàng đơn vị
		case 0:
			{
				if charDigit == 1 {
					if len(numDigits) > 1 && numDigits[1] != 0 && numDigits[1] != 1 {
						result += "mốt"
					} else {
						result += "một"
					}
				} else {
					if charDigit != 0 {
						result += numberMap[charDigit]
					}
				}
			}
			// hàng chục
		case 1:
			{
				if charDigit == 0 {
					if numDigits[0] != 0 {
						result += "lẻ"
						result += " "
					}
				} else if charDigit == 1 {
					result += "mười"
					result += " "
				} else {
					result += numberMap[charDigit]
					result += " "
					result += "mươi"
					result += " "
				}
			}
		// hàng trăm
		case 2:
			{
				result += numberMap[charDigit]
				result += " "
				result += "trăm"
				result += " "
			}
		}

	}

	return result
}

func main() {
	number := os.Args[1]
	// convert string int
	parseNum, _ := strconv.Atoi(number)
	numDigits := splitNumber(parseNum)

	fmt.Println(displayNumberText(numDigits))
}
