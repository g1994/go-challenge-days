package main

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"

	"github.com/olekukonko/tablewriter"
)

func randArrayOfInts(len int) []int {
	source := rand.NewSource(time.Now().UnixNano())
	r := rand.New(source)
	results := make([]int, len)

	for i := 0; i <= len-1; i++ {
		results[i] = r.Intn(999)
	}

	return results
}

func randInt(min, max int) int {
	source := rand.NewSource(time.Now().UnixNano())
	r := rand.New(source)
	return min + r.Intn(max-min)
}

func randomString(len int) string {

	bytes := make([]byte, len)

	for i := 0; i < len; i++ {
		bytes[i] = byte(randInt(97, 122))
	}

	return string(bytes)
}

func randomBoolean() bool {
	source := rand.NewSource(time.Now().UnixNano())
	r := rand.New(source)
	flag := r.Intn(2)
	return flag == 1
}

func randArrayOfBoolean(len int) []bool {
	results := make([]bool, len)

	for i := 0; i <= len-1; i++ {
		results[i] = randomBoolean()
	}

	return results
}

func randArrayOfString(len int) []string {
	results := make([]string, len)

	for i := 0; i <= len-1; i++ {
		results[i] = randomString(6)
	}

	return results
}

func randArrayOfFloat(len int) []float64 {
	source := rand.NewSource(time.Now().UnixNano())
	r := rand.New(source)
	results := make([]float64, len)

	for i := range results {
		results[i] = 0 + r.Float64()*(999-0)
	}
	return results
}

func getData(colMap map[string]string, rowLength int) [][]string {
	data := make([][]string, 0)

	for _, val := range colMap {
		switch val {
		case "int":
			{
				arrOfInt := randArrayOfInts(rowLength)
				var arrStr []string
				for i := 0; i < len(arrOfInt); i++ {
					arrStr = append(arrStr, strconv.Itoa(arrOfInt[i]))
				}

				data = append(data, arrStr)
			}
		case "string":
			{
				arrOfString := randArrayOfString(rowLength)
				var arrStr []string

				for i := 0; i < len(arrOfString); i++ {
					arrStr = append(arrStr, arrOfString[i])
				}

				data = append(data, arrStr)
			}
		case "bool":
			{
				arrOfBool := randArrayOfBoolean(rowLength)
				var arrStr []string
				for i := 0; i < len(arrOfBool); i++ {
					if arrOfBool[i] {
						arrStr = append(arrStr, "true")
					} else {
						arrStr = append(arrStr, "false")
					}
				}

				data = append(data, arrStr)
			}
		case "float":
			{
				arrOfFloat := randArrayOfFloat(rowLength)
				var arrStr []string
				for i := 0; i < len(arrOfFloat); i++ {
					arrStr = append(arrStr, strconv.FormatFloat(arrOfFloat[i], 'E', -1, 32))
				}

				data = append(data, arrStr)
			}
		default:
			fmt.Println("Invalid value")
		}
	}

	return data
}

func main() {
	args := os.Args[1:]
	rowLength, _ := strconv.Atoi(args[0])
	colMap := make(map[string]string)
	colName := make([]string, 0)

	for i := 1; i <= len(args)-1; i += 2 {
		// get type
		colMap[args[i]] = args[i+1]

		// get column name
		colName = append(colName, args[i])
	}

	data := getData(colMap, rowLength)

	fmt.Println("----------")
	// render table
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader(colName)

	for i := 0; i < rowLength; i++ {
		row := make([]string, 0)
		for j := 0; j < len(data); j++ {
			row = append(row, data[j][i])
		}
		table.Append(row)
	}
	table.Render()
}
