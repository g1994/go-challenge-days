package main

import (
	"encoding/csv"
	"fmt"
	"os"
	"strconv"
	"time"
)

type employee struct {
	id          string
	firstName   string
	middleName  string
	lastName    string
	birthday    time.Time
	talentPoint int
	salary      float64
	gender      string
}

func main() {
	args := os.Args[1:]
	rawData, err := readCsvFile(args[0])

	data := getEmployeesFromCSV(rawData, err)

	fmt.Println("Tổng số nhân viên : ", len(data))
	printEmployeeByGender(data)
	printAvgAge(data)
}

func readCsvFile(filepath string) ([][]string, error) {
	csvFile, err := os.Open(filepath)

	defer csvFile.Close()

	csvLines, err := csv.NewReader(csvFile).ReadAll()

	return csvLines, err
}

func getEmployeesFromCSV(data [][]string, err error) []employee {
	empData := make([]employee, 0)

	if err != nil {
		fmt.Println("File not found")
		fmt.Println(err)
		os.Exit(1)
	}

	for _, line := range data {
		birthday, _ := time.Parse("2006-01-02", line[4])
		talentPoint, _ := strconv.Atoi(line[5])
		salary, _ := strconv.ParseFloat(line[6], 64)

		emp := employee{
			id:          line[0],
			firstName:   line[1],
			middleName:  line[2],
			lastName:    line[3],
			birthday:    birthday,
			talentPoint: talentPoint,
			salary:      salary,
			gender:      line[7],
		}
		empData = append(empData, emp)
	}

	return empData
}

func printEmployeeByGender(data []employee) {
	var maleNum int
	var femaleNum int
	var otherGenderNum int

	for _, employee := range data {
		gender := employee.gender
		switch gender {
		case "Nam":
			maleNum++
		case "Nữ":
			femaleNum++
		case "Khác":
			otherGenderNum++
		default:
			fmt.Println("Invalid gender")
		}
	}

	fmt.Println("Tổng số nhân viên theo giới tính:")
	fmt.Println("Nam: ", maleNum)
	fmt.Println("Nữ: ", femaleNum)
	fmt.Println("Khác: ", otherGenderNum)
}

func printAvgAge(data []employee) {
	var tolAge int
	var avgAge float64
	now := time.Now()
	yearNow := now.Year()

	for _, employee := range data {
		birthYear := employee.birthday.Year()
		tolAge += int(yearNow - birthYear)
	}

	avgAge = float64(tolAge) / float64(len(data))

	fmt.Printf("Độ tuổi trung bình của nhân viên: %.2f", avgAge)
	fmt.Println()
}
