package main

import (
	"fmt"

	"github.com/xuri/excelize/v2"
)

// 1. Số Phần (Part) của đề thi.
// 2. Số câu hỏi của từ Part.
// 3. Danh sách các câu hỏi và các mục trải lời (Answer options).

type question struct {
	id           string
	questionName string
	answer       string
	answerA      string
	answerB      string
	answerC      string
	answerD      string
	feedbackA    string
	feedbackB    string
	feedbackC    string
	feedbackD    string
}

func main() {
	// read xlsx file
	f, err := excelize.OpenFile("TOEIC_Exam.xlsx")

	if err != nil {
		fmt.Println(err)
		return
	}
	// get all part of exam file
	parts := f.GetSheetList()
	fmt.Println("Number of Part:", len(parts))

	for indexPart, part := range parts {
		fmt.Println("Part:", part)
		questions := make([]question, 0)
		rows, err := f.GetRows(part)

		if err != nil {
			fmt.Println(err)
			return
		}

		// declare index of type struct question
		var questionNameIdx int
		var answerIdx int
		var answerAIdx int
		var answerBIdx int
		var answerCIdx int
		var answerDIdx int
		var feedbackAIdx int
		var feedbackBIdx int
		var feedbackCIdx int
		var feedbackDIdx int

		for index, col := range rows[0] {

			switch col {
			case "Question":
				questionNameIdx = index
			case "Answer":
				answerIdx = index
			case "A":
				answerAIdx = index
			case "B":
				answerBIdx = index
			case "C":
				answerCIdx = index
			case "D":
				answerDIdx = index
			case "Feedback_A":
				{
					// part 5
					if indexPart == 4 {
						feedbackAIdx = 7
					} else {
						feedbackAIdx = index
					}
				}
			case "Feedback_B":
				{
					// part 5
					if indexPart == 4 {
						feedbackBIdx = 8
					} else {
						feedbackBIdx = index
					}
				}
			case "Feedback_C":
				{
					if indexPart == 4 {
						feedbackCIdx = 9
					} else {
						feedbackCIdx = index
					}
				}
			case "Feedback_D":
				{
					if indexPart == 4 {
						feedbackDIdx = 10
					} else {
						feedbackDIdx = index
					}
				}
			default:
				fmt.Println("x")
			}
		}

		for i := 1; i < len(rows); i++ {
			ques := question{
				id:           rows[i][questionNameIdx],
				questionName: rows[i][questionNameIdx],
				answer:       rows[i][answerIdx],
				answerA:      rows[i][answerAIdx],
				answerB:      rows[i][answerBIdx],
				answerC:      rows[i][answerCIdx],
				answerD:      rows[i][answerDIdx],
				feedbackA:    rows[i][feedbackAIdx],
				feedbackB:    rows[i][feedbackBIdx],
				feedbackC:    rows[i][feedbackCIdx],
				feedbackD:    rows[i][feedbackDIdx],
			}

			questions = append(questions, ques)
		}

		fmt.Println("Length of questions of part", part, "is", len(questions))

		for index, q := range questions {
			fmt.Println("Question", index)
			fmt.Println(q.questionName)
			fmt.Println("----------Options------")
			if indexPart == 0 || indexPart == 1 {
				fmt.Println("A:", q.feedbackA)
				fmt.Println("B:", q.feedbackB)
				fmt.Println("C:", q.feedbackC)
				fmt.Println("D:", q.feedbackD)
			} else {
				fmt.Println("A:", q.answerA)
				fmt.Println("B:", q.answerB)
				fmt.Println("C:", q.answerC)
				fmt.Println("D:", q.answerD)
			}
		}

	}
}

// Close the spreadsheet.
// if err = f.Close(); err != nil {
// 	fmt.Println(err)
// }
